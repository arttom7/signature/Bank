package bankdb.modelDto;

public class UserLoggingDataDto {
    private String password;
    private Long pESEL;
    private Integer noLogging;

    public UserLoggingDataDto() {
    }

    public UserLoggingDataDto(String password, Long pESEL, Integer noLogging) {
        this.password = password;
        this.pESEL = pESEL;
        this.noLogging = noLogging;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getpESEL() {
        return pESEL;
    }

    public void setPESEL(Long pESEL) {
        this.pESEL = pESEL;
    }

    public Integer getNoLogging() {
        return noLogging;
    }

    public void setNoLogging(Integer noLogging) {
        this.noLogging = noLogging;
    }
}
