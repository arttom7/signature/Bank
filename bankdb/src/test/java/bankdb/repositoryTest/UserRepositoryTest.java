package bankdb.repositoryTest;

import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import bankdb.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserRepositoryTest {

    @MockBean
    private UserRepository userRepositoryMock;
    private static List<Account> accountList = new ArrayList<>();

    private static String password = "Password";
    private static Long PESEL = 12234312245L;
    private static Integer logNumber = 98765432;

    @Before
    public void setUp() {
        Account account = new Account();
        account.setBalance(new BigDecimal(400));

        accountList.add(account);

        User user = new User();
        user.setFirstName("Artur");

        Mockito.when(userRepositoryMock.findByAccountList(accountList)).thenReturn(Optional.of(user));
        Mockito.when(userRepositoryMock.findById(1l)).thenReturn(Optional.ofNullable(user));
        Mockito.when(userRepositoryMock.findByPasswordAndPESELAndLogNumber(password, PESEL, logNumber)).thenReturn(Optional.ofNullable(user));
    }

    @Test
    public void shouldFindUserByIdWhenCorrectIdIsGiven() {
        User user = userRepositoryMock.findById(1L).get();
        Assert.assertEquals("Artur", user.getFirstName());
    }

    @Test
    public void shouldNotFindUserByIdWhenInCorrectIdIsGiven() {
        Optional<User> user = userRepositoryMock.findById(-5L);
        Assert.assertEquals(Optional.empty(), user);
    }

    @Test
    public void shouldReturnUserWhenCorrectAccountListIsGiven() {
        User user = userRepositoryMock.findByAccountList(accountList).get();
        Assert.assertEquals("Artur", user.getFirstName());
    }

    @Test
    public void shouldNotReturnUserWhenEmptyAccountListIsGiven() {
        List<Account> accountList2 = new ArrayList<>();
        Optional<User> user = userRepositoryMock.findByAccountList(accountList2);
        Assert.assertEquals(Optional.empty(), user);
    }

    @Test
    public void shouldReturnUserWhenPasswordAndPESELAndLogNumberAreAllCorrect() {
        User user = userRepositoryMock.findByPasswordAndPESELAndLogNumber(password, PESEL, logNumber).get();
        Assert.assertEquals("Artur", user.getFirstName());
    }

    @Test
    public void shouldNotReturnUserWhenPasswordAndPESELAndLogNumberAreGivenAndPasswordIsIncorrect() {
        password = "failPassword";
        Optional<User> user = userRepositoryMock.findByPasswordAndPESELAndLogNumber(password, PESEL, logNumber);
        Assert.assertEquals(Optional.empty(), user);
    }

    @Test
    public void shouldNotReturnUserWhenPasswordAndPESELAndLogNumberAreGivenAndPESELIsIncorrect() {
        PESEL = 11213212211L;
        Optional<User> user = userRepositoryMock.findByPasswordAndPESELAndLogNumber(password, PESEL, logNumber);
        Assert.assertEquals(Optional.empty(), user);
    }

    @Test
    public void shouldNotReturnUserWhenPasswordAndPESELAndLogNumberAreGivenAndLogNumberIsIncorrect() {
        logNumber = 1121321221;
        Optional<User> user = userRepositoryMock.findByPasswordAndPESELAndLogNumber(password, PESEL, logNumber);
        Assert.assertEquals(Optional.empty(), user);
    }
}
