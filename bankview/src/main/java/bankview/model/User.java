package bankview.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Object User represents user who is registered
 *
 * @author Artur Tomaszewski
 * @version 1.0.0.0
 */
public class User {
    /**
     * User's id
     */
    private Long id;
    /**
     * User's firstName
     */
    @NotNull
    @Size(min = 3, max = 30)
    private String firstName;
    /**
     * User's secondName
     */
    @NotNull
    @Size(min = 3, max = 30)
    private String secondName;
    /**
     * User's email
     */
    @NotNull
    private String email;
    /**
     * User's password
     */
    @NotNull
    private String password;
    /**
     * User's phoneNumber
     */
    @NotNull
    private Long phoneNumber;
    /**
     * User's PESEL
     */
    @NotNull
    private Long PESEL;
    /**
     * User's logNumber. Number which is neccessary during logging
     */
    private Integer logNumber;
    /**
     * User's registration Date
     */
    private Date registrationDate;

    /**
     * Default Constructor
     */
    public User() {
    }

    /**
     * Constructor that makes object User with firstName, secondName, email, password, phoneNumber, PESEL<br>
     * Each param must not be null
     *
     * @param firstName   sets firstName
     * @param secondName  sets secondName
     * @param email       sets email
     * @param password    sets password
     * @param phoneNumber sets phoneNumber
     * @param PESEL       sets PESEL
     */
    public User(@NotNull @Size(min = 3, max = 30) String firstName, @NotNull @Size(min = 3, max = 30) String secondName, @NotNull String email, @NotNull String password, @NotNull Long phoneNumber, @NotNull Long PESEL) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.PESEL = PESEL;
    }

    /**
     * @return User's registration date in Date type
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate sets registrationDate in UserMapper
     * @see bankView.dtoMapper.UserMapper
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return User's logging number in Integer type
     */
    public Integer getLogNumber() {
        return logNumber;
    }

    /**
     * @param logNumber sets logNumber in UserMapper
     * @see bankView.dtoMapper.UserMapper
     */
    public void setLogNumber(Integer logNumber) {
        this.logNumber = logNumber;
    }

    /**
     * @return User's id Long type
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id sets id in UserMappper
     * @see bankView.dtoMapper.UserMapper
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return User's phoneNumber in Long type
     */
    public Long getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber sets phoneNumber
     * @see bankView.dtoMapper.UserMapper
     */
    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return User's PESEL in Long type
     */
    public Long getPESEL() {
        return PESEL;
    }

    /**
     * @param PESEL sets PESEL
     * @see bankView.dtoMapper.UserMapper
     */
    public void setPESEL(Long PESEL) {
        this.PESEL = PESEL;
    }

    /**
     * @return User's firstName in String type
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName sets firstName
     * @see bankView.dtoMapper.UserMapper
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return User's secondName in String type
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * @param secondName sets secondName
     * @see bankView.dtoMapper.UserMapper
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * @return User's email in String type
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email sets Email
     * @see bankView.dtoMapper.UserMapper
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return User's password in String type
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password sets password
     * @see bankView.dtoMapper.UserMapper
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
