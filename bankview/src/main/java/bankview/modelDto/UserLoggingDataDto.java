package bankview.modelDto;

public class UserLoggingDataDto {
    private String password;
    private Long PESEL;
    private Integer noLogging;

    public UserLoggingDataDto(String password, Long PESEL, Integer noLogging) {
        this.password = password;
        this.PESEL = PESEL;
        this.noLogging = noLogging;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getPESEL() {
        return PESEL;
    }

    public void setPESEL(Long PESEL) {
        this.PESEL = PESEL;
    }

    public Integer getNoLogging() {
        return noLogging;
    }

    public void setNoLogging(Integer noLogging) {
        this.noLogging = noLogging;
    }
}
