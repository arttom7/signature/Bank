package bankdb.modelDB;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Object User represents user who is registered
 *
 * @author Artur Tomaszewski
 * @version 1.0.0.0
 */
@Entity
public class User {
    /**
     * User's id, auto generated
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * User's firstName
     */
    @NotNull
    @Size(min = 2, max = 30)
    private String firstName;
    /**
     * User's secondName
     */
    @NotNull
    @Size(min = 2, max = 30)
    private String secondName;
    /**
     * User's email
     */
    @NotNull
    private String email;
    /**
     * User's password
     */
    @NotNull
    private String password;
    /**
     * User's registration date
     */
    private Date registrationDate;
    /**
     * User's PESEL
     */
    @NotNull
    private Long pESEL;
    /**
     * User's phoneNumber
     */
    @NotNull
    private Long phoneNumber;
    /**
     * User's logNumber
     */
    private Integer logNumber;
    /**
     * User's list of accounts
     */
    @OneToMany
    private List<Account> accountList;
    /**
     * User's list of overflows
     */
    @ManyToMany
    private List<Overflow> overflowsList;

    /**
     * Default constructor
     */
    public User() {
    }

    /**
     * Constructor that makes User object with firstName, secondName, email, password, registration date, PESEL, phoneNumber, logNumber, accountList,overflowsList
     *
     * @param firstName        sets User's firstName
     * @param secondName       sets User's secondName
     * @param email            sets User's email
     * @param password         sets User's password
     * @param registrationDate sets User's registration date
     * @param pESEL            sets User's PESEL
     * @param phoneNumber      sets User's phoneNumber
     * @param logNumber        sets User's logNumber
     * @param accountList      sets User's accountList
     * @param overflowsList    sets User's overflowList
     */
    public User(@NotNull String firstName, @NotNull String secondName, @NotNull String email, @NotNull String password, Date registrationDate, @NotNull Long pESEL, @NotNull Long phoneNumber, Integer logNumber, List<Account> accountList, List<Overflow> overflowsList) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.registrationDate = registrationDate;
        this.pESEL = pESEL;
        this.phoneNumber = phoneNumber;
        this.logNumber = logNumber;
        this.accountList = accountList;
        this.overflowsList = overflowsList;
    }

    /**
     * @param overflow sets User's overflows list
     */
    public void setOverflowsList(Overflow overflow) {
        this.overflowsList.add(overflow);
    }

    /**
     * @return User's registration date in Date type
     */
    public Date getRegistrationDate() {
        return registrationDate;
    }

    /**
     * @param registrationDate sets User's registration date
     * @see bankDB.dtoMapper.UserMapper
     * @see bankDB.service.UserService when creating new user, this method is used
     */
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    /**
     * @return User's PESEL in Long type
     */
    public Long getPESEL() {
        return pESEL;
    }

    /**
     * @param PESEL sets User's PESEL
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setPESEL(Long PESEL) {
        this.pESEL = PESEL;
    }

    /**
     * @return User's phoneNumber in Long type
     */
    public Long getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber sets User's phoneNumber
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @param account adds Account to User's accounts list
     * @see bankDB.service.AccountService during creating a bank account this method is used
     */
    public void setAccountList(Account account) {
        this.accountList.add(account);
    }

    /**
     * @param accountList sets User's accountList
     */
    public void setAccount(List<Account> accountList) {
        this.accountList = accountList;
    }

    /**
     * @return User's logNumber in Integer type
     */
    public Integer getLogNumber() {
        return logNumber;
    }

    /**
     * @param logNumber sets User's logNnumber
     * @see bankDB.service.UserService creation of logNumber
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setLogNumber(Integer logNumber) {
        this.logNumber = logNumber;
    }

    /**
     * @return User's id in Long type
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id sets User's id
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return User's firstName in String type
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName sets User's firstName
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return User's secondName in String type
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * @param secondName sets User's secondName
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * @return User's email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email sets User's email
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return User's password in String type
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password sets User's password
     * @see bankDB.dtoMapper.UserMapper
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public void hello() {
        System.out.println("Hello World");
    }
}
