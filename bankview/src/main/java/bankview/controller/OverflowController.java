package bankview.controller;

import bankview.model.Overflow;
import bankview.service.OverflowService;
import bankview.util.ViewUrls;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

@Controller
public class OverflowController {

    private OverflowService overflowService;

    public OverflowController(OverflowService overflowService) {
        this.overflowService = overflowService;
    }

    @PostMapping(path = "/login/createOverflow")
    public String createOverflow(@RequestParam Long senderNumber, @RequestParam String receiverName,
                                 @RequestParam String receiverStreet, @RequestParam String receiverCity,
                                 @RequestParam Long receiverNumber, @RequestParam String overflowName,
                                 @RequestParam BigDecimal amount, HttpSession session) {
        String status = overflowService.createOverflow(new Overflow(senderNumber, receiverNumber, receiverStreet, receiverName, receiverCity, overflowName, amount));
        session.setAttribute("OverflowStatus", status);
        return ViewUrls.LOGGEDHOME_PAGE;
    }
}
