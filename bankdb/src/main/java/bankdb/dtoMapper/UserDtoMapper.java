package bankdb.dtoMapper;

import bankdb.modelDB.User;
import bankdb.modelDto.UserDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserDtoMapper implements Function<User, UserDto> {

    @Override
    public UserDto apply(User user) {
        UserDto userDto = new UserDto();
        userDto.setFirstName(user.getFirstName());
        userDto.setSecondName(user.getSecondName());
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setPESEL(user.getPESEL());
        userDto.setId(user.getId());
        userDto.setLogNumber(user.getLogNumber());
        userDto.setRegistrationDate(user.getRegistrationDate());
        return userDto;
    }
}
