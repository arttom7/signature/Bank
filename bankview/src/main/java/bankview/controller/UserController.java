package bankview.controller;

import bankview.model.Account;
import bankview.model.User;
import bankview.modelDto.UserLoggingDataDto;
import bankview.service.UserService;
import bankview.util.ViewUrls;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/")
    public String getHomePage() {
        return ViewUrls.HOME_PAGE;
    }

    @GetMapping(path = "/registration")
    public String getRegistrationPage(User user) {
        return ViewUrls.REGISTRATION_PAGE;
    }

    @GetMapping(path = "/logging")
    public String getLoggingPage() {
        return ViewUrls.LOGGING_PAGE;
    }

    @GetMapping(path = "/login")
    public String getLoggedPage(HttpSession session) {
        session.removeAttribute("OverflowStatus");
        return ViewUrls.LOGGEDHOME_PAGE;
    }

    @GetMapping(path = "/login/accountSettings")
    public String getAccountSettingsPage() {
        return ViewUrls.ACCOUNT_SETTINGS_PAGE;
    }

    @GetMapping(path = "/logOut")
    public String getLogOutPage() {
        userService.logOut();
        return ViewUrls.HOME_PAGE;
    }

    @GetMapping(path = "/contact")
    public String getContactPage() {
        return ViewUrls.CONTACT_PAGE;
    }

//    @GetMapping(path = "/login/myBankAccounts")
//    public String getMyAccountsPage(Model model, Pageable pageable) {
//        List<Account> accountList = userService.getBankAccountsList();
//        model.addAttribute("myBankAccountList", accountList);
//        return ViewUrls.MY_BANK_ACCOUNTS_PAGE;
//    }

    @GetMapping(path = "/login/myBankAccounts")
    public String getMyAccountsPage(Model model, @RequestParam("page")Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size) {

        int currenctPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Account> accountList = userService.getBankAccountsList(PageRequest.of(currenctPage - 1, pageSize));
        model.addAttribute("myBankAccountList", accountList);

        int totalPages = accountList.getTotalPages();
        if(totalPages > 0){
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }


        return ViewUrls.MY_BANK_ACCOUNTS_PAGE;
    }

    @PostMapping(path = "/addNewUser")
    public String addNewUser(@Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ViewUrls.REGISTRATION_PAGE;
        } else {
            userService.addNewUser(user);
            return ViewUrls.HOME_PAGE;
        }
    }

    @PutMapping(path = "/login/updateAccount")
    public String updateUser(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "secondName") String secondName,
                             @RequestParam(name = "email") String email, @RequestParam(name = "password") String password,
                             @RequestParam(name = "phoneNumber") Long phoneNumber, @RequestParam(name = "PESEL") Long PESEL) {
        User user = new User(firstName, secondName, email, password, phoneNumber, PESEL);
        userService.changeAccountSettings(user);
        return ViewUrls.LOGGEDHOME_PAGE;
    }

    @PostMapping(path = "/login")
    public String logUser(HttpSession session, @RequestParam(name = "password") String password,
                          @RequestParam(name = "PESEL") Long PESEL,
                          @RequestParam(name = "noLogging") Integer noLogging) {
        User user = userService.logging(new UserLoggingDataDto(password, PESEL, noLogging));
        if (user.getFirstName() == null) {
            return ViewUrls.LOGGING_PAGE;
        } else {
            session.setAttribute("loggedUser", user);
            return ViewUrls.LOGGEDHOME_PAGE;
        }
    }

    @DeleteMapping(path = "/login/deleteAccount")
    public String deleteAccount() {
        userService.deleteAccount();
        return ViewUrls.HOME_PAGE;
    }
}
