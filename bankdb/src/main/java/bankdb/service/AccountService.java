package bankdb.service;

import bankdb.dtoMapper.AccountMapper;
import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import bankdb.modelDto.AccountDto;
import bankdb.repository.AccountRepository;
import bankdb.repository.UserRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class AccountService {

    private UserRepository userRepository;
    private AccountMapper accountMapper;
    private AccountRepository accountRepository;

    public AccountService(UserRepository userRepository, AccountMapper accountMapper, AccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.accountMapper = accountMapper;
        this.accountRepository = accountRepository;
    }

    public void createBankAccount(AccountDto accountDto, Long id) {
        User user = userRepository.findById(id).get();
        Account account = accountMapper.apply(accountDto);
        account.setCreationDate(new Date());
        account.setNumber(createAccountNumber());
        account.setUser(user);

        user.setAccountList(account);
        accountRepository.save(account);
        System.out.println("hello");
        System.out.println(user.getFirstName());
    }

    public Long createAccountNumber() {
        Random random = new Random();
        long downRange = 99999999999999999L;
        long upRange = 999999999999999999L;
        Long number = downRange + random.nextLong() * (upRange - downRange);
        if (number < 0) {
            number = number * (-1);
        }
        return number;
    }

    @Scheduled(cron = "1 0 0 1 * ?")
    public void updateBalanceByRate() {
        List<Account> accountList = (List<Account>) accountRepository.findAll();
        for (Account account : accountList) {
            BigDecimal newBalance = account.getBalance().multiply(new BigDecimal(1.05));
            account.setBalance(newBalance);
            accountRepository.save(account);
        }
    }

    public void deleteAccounts(Long id) {
        User user = userRepository.findById(id).get();
        List<Account> accountList = accountRepository.findByUser(user);
        accountList.stream().forEach(account -> accountRepository.delete(account));
    }


}
