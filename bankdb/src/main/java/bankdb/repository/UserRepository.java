package bankdb.repository;

import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByPasswordAndPESELAndLogNumber(String password, Long pESEL, Integer logNumber);

    Optional<User> findById(Long id);

    Optional<User> findByAccountList(List<Account> account);

}
