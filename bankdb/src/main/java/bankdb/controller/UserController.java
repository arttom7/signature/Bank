package bankdb.controller;

import bankdb.dtoMapper.UserDtoMapper;
import bankdb.modelDto.AccountDto;
import bankdb.modelDto.UserDto;
import bankdb.modelDto.UserLoggingDataDto;
import bankdb.repository.UserRepository;
import bankdb.service.AccountService;
import bankdb.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
public class UserController {

    private UserRepository userRepository;
    private UserService userService;
    private UserDtoMapper userDtoMapper;
    private AccountService accountService;

    public UserController(UserRepository userRepository, UserService userService, UserDtoMapper userDtoMapper, AccountService accountService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.userDtoMapper = userDtoMapper;
        this.accountService = accountService;
    }

    @PostMapping(path = "/addNewUser")
    public void addNewUser(@RequestBody UserDto userDto) {
        userService.addNewUser(userDto);
    }

    @PostMapping(path = "/login")
    public UserDto loggingUser(@RequestBody UserLoggingDataDto userLoggingDataDto) {
        UserDto userDto = new UserDto();
        try {
            userDto = userDtoMapper.apply(userRepository.findByPasswordAndPESELAndLogNumber(
                    userLoggingDataDto.getPassword(), userLoggingDataDto.getpESEL(),
                    userLoggingDataDto.getNoLogging()).get());
        } catch (NoSuchElementException e) {
        }
        return userDto;
    }

    @PutMapping(path = "/login/updateAccount")
    public void updateAccount(@RequestBody UserDto userDto) {
        userService.updateAccount(userDto);
    }

    @DeleteMapping(path = "/login/deleteAccount/{id}")
    public void deleteAccount(@PathVariable Long id) {
        accountService.deleteAccounts(id);
        userService.deleteAccount(id);
    }

    @GetMapping(path = "/login/getBankAccount/{id}")
    public List<AccountDto> getBankAccountsList(@PathVariable Long id) {
        List<AccountDto> accountDtoList = userService.getBankAccountsList(id);
        return accountDtoList;
    }
}
