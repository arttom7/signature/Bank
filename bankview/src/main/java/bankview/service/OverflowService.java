package bankview.service;

import bankview.dtoMapper.OverflowDtoMapper;
import bankview.dtoMapper.OverflowMapper;
import bankview.model.Overflow;
import bankview.model.User;
import bankview.modelDto.OverflowDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Service
public class OverflowService {

    private final String CREATE_OVERFLOW_URL = "http://localhost:8080/login/createOverflow";
    private final String GET_OVERFLOWS_URL = "http://localhost:8080/login/getOverflows/";
    private OverflowDtoMapper overflowDtoMapper;
    private OverflowMapper overflowMapper;
    private HttpSession session;

    public OverflowService(OverflowDtoMapper overflowDtoMapper, OverflowMapper overflowMapper, HttpSession session) {
        this.overflowDtoMapper = overflowDtoMapper;
        this.overflowMapper = overflowMapper;
        this.session = session;
    }

    public String createOverflow(Overflow overflow) {
        OverflowDto overflowDto = overflowDtoMapper.apply(overflow);
        RestTemplate restTemplate = new RestTemplate();
        String status = restTemplate.postForObject(CREATE_OVERFLOW_URL, overflowDto, String.class);
        return status;
    }

    public void getOverflows() {
        RestTemplate restTemplate = new RestTemplate();
        User user = (User) session.getAttribute("loggedUser");
        Long userId = user.getId();
        String url = GET_OVERFLOWS_URL + userId;

        ResponseEntity<List<OverflowDto>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<OverflowDto>>() {
                });
        List<OverflowDto> overflowDtoList = response.getBody();
        List<Overflow> overflowList = new ArrayList<>();
        overflowDtoList.stream().forEach(overflowDto -> overflowList.add(overflowMapper.apply(overflowDto)));
        session.setAttribute("financeHistoryList", overflowList);
    }
}
