package bankdb.modelDB;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Overflow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long senderNumber;

    @NotNull
    private Long receiverNumber;

    private String status;

    @NotNull
    private String receiverName;

    private String receiverStreet;

    private String receiverCity;

    @NotNull
    private String overflowName;

    private Date date;

    @NotNull
    private BigDecimal amount;

    @ManyToMany
    private List<User> userList = new ArrayList<>();

    @ManyToMany
    private List<Account> accountsList = new ArrayList<>();

    public Overflow() {
    }

    public Overflow(@NotNull Long senderNumber, @NotNull Long receiverNumber, @NotNull String receiverName, String receiverStreet, String receiverCity, @NotNull String overflowName, @NotNull BigDecimal amount) {
        this.senderNumber = senderNumber;
        this.receiverNumber = receiverNumber;
        this.receiverName = receiverName;
        this.receiverStreet = receiverStreet;
        this.receiverCity = receiverCity;
        this.overflowName = overflowName;
        this.amount = amount;
    }

    public Overflow(@NotNull Long senderNumber, @NotNull Long receiverNumber, @NotNull String receiverName, String receiverStreet, String receiverCity, @NotNull String overflowName, Date date, @NotNull BigDecimal amount, List<User> userList, List<Account> accountsList) {
        this.senderNumber = senderNumber;
        this.receiverNumber = receiverNumber;
        this.receiverName = receiverName;
        this.receiverStreet = receiverStreet;
        this.receiverCity = receiverCity;
        this.overflowName = overflowName;
        this.date = date;
        this.amount = amount;
        this.userList = userList;
        this.accountsList = accountsList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public void setAccountsList(List<Account> accountsList) {
        this.accountsList = accountsList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(User user) {
        this.userList.add(user);
        user.setOverflowsList(this);
    }

    public List<Account> getAccountsList() {
        return accountsList;
    }

    public void setAccountsList(Account account) {
        this.accountsList.add(account);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(Long senderNumber) {
        this.senderNumber = senderNumber;
    }

    public Long getReceiverNumber() {
        return receiverNumber;
    }

    public void setReceiverNumber(Long receiverNumber) {
        this.receiverNumber = receiverNumber;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverStreet() {
        return receiverStreet;
    }

    public void setReceiverStreet(String receiverStreet) {
        this.receiverStreet = receiverStreet;
    }

    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }

    public String getOverflowName() {
        return overflowName;
    }

    public void setOverflowName(String overflowName) {
        this.overflowName = overflowName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
