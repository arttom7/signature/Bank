package bankdb.dtoMapper;

import bankdb.modelDB.User;
import bankdb.modelDto.UserDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserMapper implements Function<UserDto, User> {
    @Override
    public User apply(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setSecondName(userDto.getSecondName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setPhoneNumber(userDto.getPhoneNumber());
        user.setPESEL(userDto.getPESEL());
        user.setId(userDto.getId());
        user.setLogNumber(userDto.getLogNumber());
        user.setRegistrationDate(userDto.getRegistrationDate());
        return user;
    }
}
