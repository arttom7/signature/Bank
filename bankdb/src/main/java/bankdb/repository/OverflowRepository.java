package bankdb.repository;

import bankdb.modelDB.Overflow;
import bankdb.modelDB.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OverflowRepository extends CrudRepository<Overflow, Long> {
    List<Overflow> findByUserListContaining(User user);
}
