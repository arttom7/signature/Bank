package bankdb.repositoryTest;

import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import bankdb.repository.AccountRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRepositoryTest {

    @MockBean
    private AccountRepository accountRepository;

    private Long number = 99999999999999999L;

    private User user = new User();

    @Before
    public void setUp() {
        Account account = new Account();
        account.setBalance(new BigDecimal(500));

        List<Account> accountList = Arrays.asList(account);

        Mockito.when(accountRepository.findByNumber(number)).thenReturn(account);
        Mockito.when(accountRepository.findAllByNumber(number)).thenReturn(accountList);
        Mockito.when(accountRepository.findByUser(user)).thenReturn(accountList);
    }

    @Test
    public void shouldReturnAccountsListWhenGivenNumberIsCorrect() {
        Account account = accountRepository.findByNumber(number);
        Assert.assertEquals(new BigDecimal(500), account.getBalance());
    }

    @Test
    public void shouldNotReturnAccountsListWhenGivenNumberIsIncorrect() {
        number = 0L;
        Account account = accountRepository.findByNumber(number);
        Assert.assertEquals(null, account);
    }

    @Test
    public void shouldReturnListOfAccountWhenGivenNumberIsCorrect() {
        List<Account> accountList = accountRepository.findAllByNumber(number);
        Assert.assertEquals(new BigDecimal(500), accountList.get(0).getBalance());
    }

    @Test
    public void shouldNotReturnListOfAccountWhenGivenNumberIsIncorrect() {
        number = 0L;
        List<Account> accountList = accountRepository.findAllByNumber(number);
        Assert.assertEquals(0, accountList.size());
    }

    @Test
    public void shouldReturnListOfAccountsWhenGivenUserIsCorrect() {
        List<Account> accountList = accountRepository.findByUser(user);
        Assert.assertEquals(new BigDecimal(500), accountList.get(0).getBalance());
    }

    @Test
    public void shouldNotReturnListOfAccountsWhenGivenUserIsIncorrect() {
        User user2 = new User();
        List<Account> accountList = accountRepository.findByUser(user2);
        Assert.assertEquals(0, accountList.size());
    }
}
