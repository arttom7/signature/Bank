package bankdb.dtoMapper;

import bankdb.modelDB.Account;
import bankdb.modelDto.AccountDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AccountMapper implements Function<AccountDto, Account> {


    @Override
    public Account apply(AccountDto accountDto) {
        Account account = new Account();
        account.setId(accountDto.getId());
        account.setBalance(accountDto.getBalance());
        account.setDepositAccount(accountDto.isDepositAccount());
        account.setNumber(accountDto.getNumber());
        account.setCreationDate(accountDto.getCreationDate());
        account.setCurrency(accountDto.getCurrency());
        account.setName(accountDto.getName());
        return account;
    }
}

