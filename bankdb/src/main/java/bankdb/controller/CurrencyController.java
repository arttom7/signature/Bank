package bankdb.controller;

import bankdb.service.CurrencyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Currency;

@RestController
public class CurrencyController {
    private CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping(path = "/convertAmount/{balance}/{currency}")
    public BigDecimal getConvertedAmount(@PathVariable BigDecimal balance, @PathVariable Currency currency) {
        BigDecimal sum = currencyService.getConvertedAmount(balance, currency);
        return sum;
    }

    @GetMapping(path = "/getExchangeRate/{currency}")
    public BigDecimal getExchangeRate(@PathVariable Currency currency) {
        BigDecimal rate = currencyService.getExchangeRate(currency);
        return rate;
    }
}
