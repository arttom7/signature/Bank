package bankview.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Currency;

@Service
public class CurrencyService  {

    private String GET_CURRENCY_URL = "http://localhost:8080/getExchangeRate/";

    public BigDecimal getExchangeRate(Currency currency) {
        String url = GET_CURRENCY_URL + currency;
        RestTemplate restTemplate = new RestTemplate();
        BigDecimal rate = restTemplate.getForObject(url, BigDecimal.class);
        return rate;
    }
}
