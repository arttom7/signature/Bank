package bankview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankviewApplication.class, args);
    }
}
