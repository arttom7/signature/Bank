package bankview.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

/**
 * <pre>
 * Object account represents user's bank account.
 * Many accounts can have one user.
 * Accounts can have only one user.
 *
 * </pre>
 *
 * @author Artur Tomaszewski
 * @version 1.0.0.0
 */
public class Account {
    /**
     * Account's name
     */
    private String name;
    /**
     * Account's id
     */
    private Long id;
    /**
     * Account's number
     */
    private Long number;
    /**
     * Account's creationDate
     */
    private Date creationDate;
    /**
     * Account's currency
     */
    private Currency currency;
    /**
     * Account's balance
     */
    private BigDecimal balance;
    /**
     * Boolean if this account is deposit or not
     */
    private boolean isDepositAccount;
    /**
     * Account's User
     */
    private User user;

    /**
     * Default constructor
     */
    public Account() {
    }

    /**
     * Constructor that makes object Account with name, id, number, creation date, currency, balance, isDepositAccount(information if is it deposit), user
     *
     * @param name             sets account's name
     * @param id               sets account's id
     * @param number           sets account's number
     * @param creationDate     sets account's creation date
     * @param currency         sets account's currency
     * @param balance          sets account's balance
     * @param isDepositAccount sets whether account is deposit
     * @param user             sets account's user
     */
    public Account(String name, Long id, Long number, Date creationDate, Currency currency, BigDecimal balance, boolean isDepositAccount, User user) {
        this.name = name;
        this.id = id;
        this.number = number;
        this.creationDate = creationDate;
        this.currency = currency;
        this.balance = balance;
        this.isDepositAccount = isDepositAccount;
        this.user = user;
    }

    /**
     * @return account's name in String type
     */
    public String getName() {
        return name;
    }

    /**
     * @param name Sets account's name
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return account's id in Long type
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id sets account's id
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return account's number in Long type
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number sets account's number
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    /**
     * @return account's creation date in Date type
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate sets account's creation date
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return account's currency in Currency type
     */
    public Currency getCurrency() {
        return currency;
    }

    /**
     * @param currency sets account's currency
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /**
     * @return account's balance in BigDecimal type
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * @param balance sets account's balance
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return boolean if account is deposit
     */
    public boolean isDepositAccount() {
        return isDepositAccount;
    }

    /**
     * @param depositAccount sets if account is deposit
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setDepositAccount(boolean depositAccount) {
        isDepositAccount = depositAccount;
    }

    /**
     * @return account's User in User type
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user sets account's User
     * @see bankView.dtoMapper.AccountMapper
     */
    public void setUser(User user) {
        this.user = user;
    }
}
