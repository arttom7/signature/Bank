package bankdb.controller;

import bankdb.dtoMapper.OverflowMapper;
import bankdb.modelDto.OverflowDto;
import bankdb.repository.OverflowRepository;
import bankdb.service.OverflowService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OverflowController {

    private OverflowRepository overflowRepository;
    private OverflowMapper overflowMapper;
    private OverflowService overflowService;

    public OverflowController(OverflowRepository overflowRepository, OverflowMapper overflowMapper, OverflowService overflowService) {
        this.overflowRepository = overflowRepository;
        this.overflowMapper = overflowMapper;
        this.overflowService = overflowService;
    }

    @PostMapping(path = "/login/createOverflow")
    public String createOverflow(@RequestBody OverflowDto overflowDto) {
        String status = overflowService.createOverflow(overflowDto);
        return status;
    }

    @GetMapping(path = "/login/getOverflows/{id}")
    public List<OverflowDto> getAllOverflows(@PathVariable Long id) {
        List<OverflowDto> overflowDtoList = overflowService.getOverflows(id);
        return overflowDtoList;
    }
}
