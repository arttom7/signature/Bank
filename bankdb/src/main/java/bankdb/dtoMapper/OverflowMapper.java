package bankdb.dtoMapper;

import bankdb.modelDB.Overflow;
import bankdb.modelDto.OverflowDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class OverflowMapper implements Function<OverflowDto, Overflow> {
    @Override
    public Overflow apply(OverflowDto overflowDto) {
        Overflow overflow = new Overflow();
        overflow.setAmount(overflowDto.getAmount());
        overflow.setDate(overflowDto.getDate());
        overflow.setOverflowName(overflowDto.getOverflowName());
        overflow.setReceiverCity(overflowDto.getReceiverCity());
        overflow.setReceiverName(overflowDto.getReceiverName());
        overflow.setReceiverNumber(overflowDto.getReceiverNumber());
        overflow.setReceiverStreet(overflowDto.getReceiverStreet());
        overflow.setSenderNumber(overflowDto.getSenderNumber());
        return overflow;
    }
}
