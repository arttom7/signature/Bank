package bankdb.controller;

import bankdb.modelDto.AccountDto;
import bankdb.service.AccountService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping(path = "/createBankAccount/{id}")
    public void createBankAccount(@RequestBody AccountDto accountDto, @PathVariable Long id) {
        accountService.createBankAccount(accountDto, id);
    }
}
