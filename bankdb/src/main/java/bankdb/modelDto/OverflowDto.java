package bankdb.modelDto;

import java.math.BigDecimal;
import java.util.Date;

public class OverflowDto {
    private Long senderNumber;
    private Long receiverNumber;
    private String receiverName;
    private String receiverStreet;
    private String receiverCity;
    private String overflowName;
    private Date date;
    private BigDecimal amount;

    public OverflowDto() {
    }

    public OverflowDto(Long senderNumber, Long receiverNumber, String receiverName, String receiverStreet, String receiverCity, String overflowName, BigDecimal amount) {
        this.senderNumber = senderNumber;
        this.receiverNumber = receiverNumber;
        this.receiverName = receiverName;
        this.receiverStreet = receiverStreet;
        this.receiverCity = receiverCity;
        this.overflowName = overflowName;
        this.amount = amount;
    }

    public Long getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(Long senderNumber) {
        this.senderNumber = senderNumber;
    }

    public Long getReceiverNumber() {
        return receiverNumber;
    }

    public void setReceiverNumber(Long receiverNumber) {
        this.receiverNumber = receiverNumber;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverStreet() {
        return receiverStreet;
    }

    public void setReceiverStreet(String receiverStreet) {
        this.receiverStreet = receiverStreet;
    }

    public String getReceiverCity() {
        return receiverCity;
    }

    public void setReceiverCity(String receiverCity) {
        this.receiverCity = receiverCity;
    }

    public String getOverflowName() {
        return overflowName;
    }

    public void setOverflowName(String overflowName) {
        this.overflowName = overflowName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
