package bankdb.service;

import bankdb.EnumsPackage.OverflowStatus;
import bankdb.dtoMapper.OverflowDtoMapper;
import bankdb.dtoMapper.OverflowMapper;
import bankdb.modelDB.Account;
import bankdb.modelDB.Overflow;
import bankdb.modelDB.User;
import bankdb.modelDto.OverflowDto;
import bankdb.repository.AccountRepository;
import bankdb.repository.OverflowRepository;
import bankdb.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
public class OverflowService {

    private OverflowRepository overflowRepository;
    private OverflowMapper overflowMapper;
    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private CurrencyService currencyService;
    private OverflowDtoMapper overflowDtoMapper;

    @Autowired
    public OverflowService(OverflowRepository overflowRepository, OverflowMapper overflowMapper, AccountRepository accountRepository, UserRepository userRepository, CurrencyService currencyService, OverflowDtoMapper overflowDtoMapper) {
        this.overflowRepository = overflowRepository;
        this.overflowMapper = overflowMapper;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
        this.currencyService = currencyService;
        this.overflowDtoMapper = overflowDtoMapper;
    }

    public String createOverflow(OverflowDto overflowDto) {
        try {
            Overflow overflow = overflowMapper.apply(overflowDto);
            List<Account> accountListSender = accountRepository.findAllByNumber(overflow.getSenderNumber());
            List<Account> accountListReceiver = accountRepository.findAllByNumber(overflow.getReceiverNumber());
            Account accountSender = accountRepository.findByNumber(overflow.getSenderNumber());
            Account accountReceiver = accountRepository.findByNumber(overflow.getReceiverNumber());
            User sender = userRepository.findByAccountList(accountListSender).get();
            User receiver = userRepository.findByAccountList(accountListReceiver).get();

            BigDecimal rate = currencyService.getExchangeRate(accountSender.getCurrency(), accountReceiver.getCurrency());

            if (checkIfAmountIsCorrect(overflow.getAmount())) {
                if (checkIfIsEnoughBalance(accountSender, overflow.getAmount())) {
                    overflow.setUserList(sender);
                    overflow.setUserList(receiver);
                    overflow.setStatus(OverflowStatus.Wykonano.getDescription());
                    accountSender.setOverflowsList(overflow);
                    accountReceiver.setOverflowsList(overflow);
                    updateBalance(accountSender, accountReceiver, overflow.getAmount(), rate);
                    overflowRepository.save(overflow);
                    return OverflowStatus.WykonanoPrzelew.getDescription();
                }
                return OverflowStatus.NieWykonanoPrzelewu.getDescription();
            }
            return OverflowStatus.BłędnaKwotaPrzelewu.getDescription();
        } catch (NoSuchElementException e) {
            Overflow overflow = overflowMapper.apply(overflowDto);
            List<Account> accountListSender = accountRepository.findAllByNumber(overflow.getSenderNumber());
            Account accountSender = accountRepository.findByNumber(overflow.getSenderNumber());
            User sender = userRepository.findByAccountList(accountListSender).get();
            overflow.setUserList(sender);
            overflow.setStatus(OverflowStatus.NieWykonano.getDescription());
            accountSender.setOverflowsList(overflow);
            overflowRepository.save(overflow);
            return OverflowStatus.NiepoprawnyNumerKontaOdbiorcy.getDescription();
        }
    }

    public boolean checkIfAmountIsCorrect(BigDecimal amount) {
        return amount.compareTo(BigDecimal.ZERO) >= 0 && amount.compareTo(BigDecimal.ZERO) != 0;
    }

    public void updateBalance(Account accountSender, Account accountReceiver, BigDecimal amount, BigDecimal rate) {
        BigDecimal receiverAmount = amount.multiply(rate);
        BigDecimal senderBalance = accountSender.getBalance();
        BigDecimal receiverBalance = accountReceiver.getBalance();
        senderBalance = senderBalance.subtract(amount);
        receiverBalance = receiverBalance.add(receiverAmount);
        accountSender.setBalance(senderBalance);
        accountReceiver.setBalance(receiverBalance);
    }

    public boolean checkIfIsEnoughBalance(Account accountSender, BigDecimal amount) {
        int res;
        res = accountSender.getBalance().compareTo(amount);
        return res == 0 || res == 1;
    }

    public List<OverflowDto> getOverflows(Long id) {
        List<OverflowDto> overflowDtoList = new ArrayList<>();
        try {
            User user = userRepository.findById(id).get();
            List<Overflow> overflowList = overflowRepository.findByUserListContaining(user);
            overflowList.stream().forEach(overflow -> overflowDtoList.add(overflowDtoMapper.apply(overflow)));
            return overflowDtoList;
        } catch (NoSuchElementException e) {
        }
        return overflowDtoList;
    }
}
