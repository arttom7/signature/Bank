package bankdb.controllerTest;

import bankdb.modelDB.User;
import bankdb.modelDto.UserDto;
import bankdb.modelDto.UserLoggingDataDto;
import bankdb.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

import static io.restassured.RestAssured.given;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @MockBean
    private UserRepository userRepository;

    private UserLoggingDataDto userLoggingDataDto = new UserLoggingDataDto("password", 12121212121L, 121212112);

    private User user = new User();

    @Before
    public void setUp() {
        user.setFirstName("Maciej");
        Mockito.when(userRepository.findByPasswordAndPESELAndLogNumber(
                userLoggingDataDto.getPassword(), userLoggingDataDto.getpESEL(),
                userLoggingDataDto.getNoLogging())).thenReturn(Optional.of(user));
    }

    @Test
    public void shouldCreateNewUserWhenCorrectDataIsGiven() {
        UserDto userDto = new UserDto("AR", "AR", "as@gmail.com", "aa", 211122321L, 12121212121L);
        given()
                .body(userDto)
                .header("content-type", "application/json")
                .post("/addNewUser")
                .then()
                .statusCode(200);
    }

    @Test
    public void shouldNotCreateNewUserWhenDataIsEmpty() {
        UserDto userDto = new UserDto();
        given()
                .body(userDto)
                .header("content-type", "application/json")
                .post("/addNewUser")
                .then()
                .statusCode(500);
    }

    @Test
    public void shouldLogUserWhenCorrectDataIsGiven() {
        given()
                .body(userLoggingDataDto)
                .header("content-type", "application/json")
                .post("/login")
                .then()
                .statusCode(200)
                .extract()
                .as(UserDto.class);
    }

    @Test
    public void shouldNotLogUserWhenCorrectDataIsIncorrect() {
        UserLoggingDataDto userLoggingDataDto2 = new UserLoggingDataDto();
        UserDto userDto = given()
                .body(userLoggingDataDto2)
                .header("content-type", "application/json")
                .post("/login")
                .then()
                .statusCode(200).extract().body().as(UserDto.class);
        Assert.assertEquals(null, userDto.getFirstName());
    }
}
