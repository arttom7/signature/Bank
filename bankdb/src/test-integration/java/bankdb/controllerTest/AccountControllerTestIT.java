package bankdb.controllerTest;

import bankdb.BankdbApplication;
import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import bankdb.modelDto.AccountDto;
import bankdb.repository.AccountRepository;
import bankdb.repository.UserRepository;
import bankdb.service.AccountService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

import static io.restassured.RestAssured.given;

@SpringBootTest(classes = BankdbApplication.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@RunWith(SpringRunner.class)
public class AccountControllerTestIT {

    @Mock
    private static AccountRepository accountRepository;

    @Mock
    private static UserRepository userRepository;

    @InjectMocks
    private static AccountService accountService;

    @InjectMocks
    private static Account account;

    @InjectMocks
    private static User user;

    @BeforeAll
    public static void setUp(){
        user.setId(2L);
        user.setFirstName("Przemek");

        account.setCreationDate(new Date());
        account.setNumber(accountService.createAccountNumber());
        account.setUser(user);

        user.setAccountList(account);

        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.ofNullable(user));
        Mockito.when(accountRepository.save(Mockito.any(Account.class))).thenReturn(account);
    }

    @Test
    public void shouldSaveAccountWithCorrectData(){
        AccountDto accountDto = new AccountDto();
        given()
                .body(accountDto)
                .header("content-type", "application/json")
                .post("/createBankAccount/2")
                .then()
                .statusCode(200);
    }

}
