package bankdb.service;

import bankdb.modelDB.Account;
import bankdb.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class BankRateService {


    Timer timer;
    Date date = new Date();
    int i;

    public BankRateService() {
        this.i = 0;
        this.timer = new Timer();
        timer.schedule(new UpdateBalance(), date, 2000);

    }

    class UpdateBalance extends TimerTask {

        @Autowired
        AccountRepository accountRepository;

        public void run() {

            System.out.println("Hello");
            Iterable<Account> accountList = accountRepository.findAll();
            System.out.println(accountList);
        }

    }
}
