package bankview.service;

import bankview.dtoMapper.AccountMapper;
import bankview.dtoMapper.UserDtoMapper;
import bankview.dtoMapper.UserMapper;
import bankview.model.Account;
import bankview.model.User;
import bankview.modelDto.AccountDto;
import bankview.modelDto.UserDto;
import bankview.modelDto.UserLoggingDataDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserService {

    private UserDtoMapper userDtoMapper;
    private UserMapper userMapper;
    private HttpSession session;
    private AccountMapper accountMapper;
    private final String addNewUserUrl = "http://localhost:8080/addNewUser";
    private final String loggingDataUrl = "http://localhost:8080/login";
    private final String changeAccountSettingsUrl = "http://localhost:8080/login/updateAccount";
    private final String deleteAccountUrl = "http://localhost:8080/login/deleteAccount/";
    private final String getBankAccountUrl = "http://localhost:8080/login/getBankAccount/";

    public UserService(UserDtoMapper userDtoMapper, UserMapper userMapper, HttpSession session, AccountMapper accountMapper) {
        this.userDtoMapper = userDtoMapper;
        this.userMapper = userMapper;
        this.session = session;
        this.accountMapper = accountMapper;
    }

    public void addNewUser(User user) {
        RestTemplate restTemplate = new RestTemplate();
        UserDto userDto = userDtoMapper.apply(user);
        restTemplate.postForObject(addNewUserUrl, userDto, UserDto.class);
    }

    public User logging(UserLoggingDataDto userLoggingDataDto) {
        RestTemplate restTemplate = new RestTemplate();
        User user = userMapper.apply(restTemplate.postForObject(loggingDataUrl, userLoggingDataDto, UserDto.class));
        return user;
    }

    public void logOut() {
        session.removeAttribute("loggedUser");
    }

    public void changeAccountSettings(User user) {
        UserDto userDto = userDtoMapper.apply(user);
        UserDto userDto2 = userDtoMapper.apply((User) session.getAttribute("loggedUser"));
        userDto.setId(userDto2.getId());
        userDto.setLogNumber(userDto2.getLogNumber());
        userDto.setRegistrationDate(userDto2.getRegistrationDate());
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(changeAccountSettingsUrl, userDto);
    }

    public void deleteAccount() {
        UserDto userDto = userDtoMapper.apply((User) session.getAttribute("loggedUser"));
        session.removeAttribute("loggedUser");
        RestTemplate restTemplate = new RestTemplate();
        String url = deleteAccountUrl + userDto.getId();
        restTemplate.delete(url);
    }

        public List<Account> getBankAccountsList() {
        UserDto userDto = userDtoMapper.apply((User) session.getAttribute("loggedUser"));

        String id = userDto.getId().toString();
        String url = getBankAccountUrl + id;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<AccountDto>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<AccountDto>>() {
                });
        List<AccountDto> accountDtoList = response.getBody();
        List<Account> accountList = new ArrayList<>();
        accountDtoList.stream().forEach(accountDto -> accountList.add(accountMapper.apply(accountDto)));
        return accountList;
    }
    public Page<Account> getBankAccountsList(Pageable pageable) {

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;

        UserDto userDto = userDtoMapper.apply((User) session.getAttribute("loggedUser"));

        String id = userDto.getId().toString();
        String url = getBankAccountUrl + id;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<AccountDto>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<AccountDto>>() {
                });
        List<AccountDto> accountDtoList = response.getBody();
        List<Account> accountList = new ArrayList<>();
        accountDtoList.stream().forEach(accountDto -> accountList.add(accountMapper.apply(accountDto)));

        List<Account> accounts;

        if (accountList.size() < startItem) {
            accounts = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, accountList.size());
            accounts = accountList.subList(startItem, toIndex);
        }
        Page<Account> accountPage = new PageImpl<Account>(accounts, PageRequest.of(currentPage, pageSize), accountList.size());


        return accountPage;
    }
}
