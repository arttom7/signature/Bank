package bankdb.dtoMapper;

import bankdb.modelDB.Overflow;
import bankdb.modelDto.OverflowDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class OverflowDtoMapper implements Function<Overflow, OverflowDto> {

    @Override
    public OverflowDto apply(Overflow overflow) {
        OverflowDto overflowDto = new OverflowDto();
        overflowDto.setAmount(overflow.getAmount());
        overflowDto.setDate(overflow.getDate());
        overflowDto.setOverflowName(overflow.getOverflowName());
        overflowDto.setReceiverName(overflow.getReceiverName());
        overflowDto.setReceiverCity(overflow.getReceiverCity());
        overflowDto.setReceiverStreet(overflow.getReceiverStreet());
        overflowDto.setReceiverNumber(overflow.getReceiverNumber());
        overflowDto.setSenderNumber(overflow.getSenderNumber());
        return overflowDto;
    }
}
