package bankview.controller;

import bankview.model.Account;
import bankview.service.AccountService;
import bankview.service.CurrencyService;
import bankview.service.OverflowService;
import bankview.service.UserService;
import bankview.util.ViewUrls;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.List;

@Controller
public class BankAccountController {

    private AccountService accountService;
    private UserService userService;
    private OverflowService overflowService;
    private CurrencyService currencyService;

    public BankAccountController(AccountService accountService, UserService userService, OverflowService overflowService, CurrencyService currencyService) {
        this.accountService = accountService;
        this.userService = userService;
        this.overflowService = overflowService;
        this.currencyService = currencyService;
    }

    @GetMapping(path = "/login/createBankAccount")
    public String getCreatingBankAccountPage() {
        return ViewUrls.CREATING_BANK_ACCOUNT_PAGE;
    }

    @GetMapping(path = "/login/history")
    public String getFinanceHistoryPage() {
        overflowService.getOverflows();
        return ViewUrls.HISTORY_PAGE;
    }

    @PostMapping(path = "/createBankAccount")
    public String createBankAccount(@RequestParam String name, @RequestParam Currency currency, @RequestParam BigDecimal balance,
                                    @RequestParam boolean isDeposit) {
        accountService.createBankAccount(name, currency, balance, isDeposit);
        return ViewUrls.LOGGEDHOME_PAGE;
    }

    @GetMapping(path = "/login/myFinance")
    public String getMyFinancePage(Model model, @RequestParam(required = false) Currency currency) {
        if (currency == null) {
            currency = Currency.getInstance("PLN");
        }
        List<Account> accountList = userService.getBankAccountsList();
        BigDecimal summedBalance = accountService.sumBalance(accountList);
        if (!currency.getCurrencyCode().equals("PLN")) {
            BigDecimal rate = currencyService.getExchangeRate(currency);
            summedBalance = summedBalance.multiply(rate);
        }
        summedBalance = summedBalance.setScale(2, RoundingMode.CEILING);
        model.addAttribute("myBankAccountList", accountList);
        model.addAttribute("summedBalance", summedBalance);
        return ViewUrls.MY_FINANCE_PAGE;
    }

    @GetMapping(path = "/createOverflow")
    public String createOverflow() {
        return ViewUrls.CREATE_OVERFLOW_PAGE;
    }
}
