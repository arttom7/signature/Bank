package bankdb.service;

import bankdb.dtoMapper.AccountDtoMapper;
import bankdb.dtoMapper.UserDtoMapper;
import bankdb.dtoMapper.UserMapper;
import bankdb.modelDB.User;
import bankdb.modelDto.AccountDto;
import bankdb.modelDto.UserDto;
import bankdb.repository.AccountRepository;
import bankdb.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class UserService {
    private UserMapper userMapper;
    private UserDtoMapper userDtoMapper;
    private UserRepository userRepository;
    private AccountRepository accountRepository;
    private AccountDtoMapper accountDtoMapper;

    public UserService(UserMapper userMapper, UserDtoMapper userDtoMapper, UserRepository userRepository, AccountRepository accountRepository, AccountDtoMapper accountDtoMapper) {
        this.userMapper = userMapper;
        this.userDtoMapper = userDtoMapper;
        this.userRepository = userRepository;
        this.accountRepository = accountRepository;
        this.accountDtoMapper = accountDtoMapper;
    }

    public void addNewUser(UserDto userDto) {
        User user = userMapper.apply(userDto);
        user.setLogNumber(createLogginNumber());
        user.setRegistrationDate(new Date());
        userRepository.save(user);
    }

    public Integer createLogginNumber() {
        Random random = new Random();
        Integer value = random.nextInt(99999999) + 9999999;
        return value;
    }

    public void updateAccount(UserDto userDto) {
        userRepository.save(userMapper.apply(userDto));
    }

    public void deleteAccount(Long id) {
        UserDto userDto = userDtoMapper.apply(userRepository.findById(id).get());
        userRepository.delete(userMapper.apply(userDto));
    }

    public List<AccountDto> getBankAccountsList(Long id) {
        User user = userRepository.findById(id).get();
        List<AccountDto> accountDtoList = new ArrayList<>();
        accountRepository.findByUser(user).forEach(account -> accountDtoList.add(accountDtoMapper.apply(account)));
        return accountDtoList;
    }

}
