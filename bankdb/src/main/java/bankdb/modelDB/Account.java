package bankdb.modelDB;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long number;

    private Date creationDate;

    private Currency currency;

    private BigDecimal balance;

    private boolean isDepositAccount;

    private String name;

    @ManyToOne
    private User user;

    @ManyToMany
    private List<Overflow> overflowsList = new ArrayList<>();

    public Account() {
    }

    public Account(String name, Long number, Date creationDate, Currency currency, BigDecimal balance, boolean isDepositAccount, User user) {
        this.number = number;
        this.creationDate = creationDate;
        this.currency = currency;
        this.balance = balance;
        this.isDepositAccount = isDepositAccount;
        this.user = user;
        this.name = name;
    }

    public List<Overflow> getOverflowsList() {
        return overflowsList;
    }

    public void setOverflowsList(Overflow overflow) {
        this.overflowsList.add(overflow);
        overflow.setAccountsList(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public boolean isDepositAccount() {
        return isDepositAccount;
    }

    public void setDepositAccount(boolean depositAccount) {
        isDepositAccount = depositAccount;
    }
}
