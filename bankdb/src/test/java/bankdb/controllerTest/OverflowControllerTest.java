package bankdb.controllerTest;

import bankdb.controller.OverflowController;
import bankdb.modelDB.Overflow;
import bankdb.modelDB.User;
import bankdb.modelDto.OverflowDto;
import bankdb.repository.OverflowRepository;
import bankdb.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class OverflowControllerTest {

    @Autowired
    private OverflowController overflowController;

    @MockBean
    private OverflowRepository overflowRepository;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void setUp() {
        User user = new User();
        user.setId(100L);
        user.setFirstName("Przemek");

        Overflow overflow = new Overflow(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1));
        List<Overflow> overflowList = Arrays.asList(overflow);
        Mockito.when(overflowRepository.findByUserListContaining(user)).thenReturn(overflowList);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.ofNullable(user));

    }

    @Test
    public void shouldCreateOverflowWhenGivenCorrectDate() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1));
        given()
                .body(overflowDto)
                .header("content-type", "application/json")
                .post("/login/createOverflow")
                .then()
                .statusCode(200);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenToBigAmount() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1000000000));
        String string = given()
                .body(overflowDto)
                .header("content-type", "application/json;charset=UTF-8")
                .post("/login/createOverflow")
                .then()
                .header("content-type", "text/plain;charset=UTF-8")
                .statusCode(200)
                .toString();
//        System.out.println(string);
//        Assert.assertEquals("Nie wykonano przelewu, nie ma wystarczających środków na koncie.",string);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenReceiverNumberIsInCorrect() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1000000000));
        given()
                .body(overflowDto)
                .header("content-type", "application/json")
                .post("/login/createOverflow")
                .then()
                .statusCode(200);
    }

    @Test
    public void shouldGetOverflowListWhenCorrectUserIdIsGiven() {
        when().
                get("/login/getOverflows/{id}", 1).
                then().
                statusCode(200);
    }

    @Test
    public void shouldNotGetOverflowListWhenINCorrectUserIdIsGiven() {
        when().
                get("/login/getOverflows/{id}", 1).
                then().
                statusCode(200);
    }

    @Test
    public void whenValidId_ListWithOverflowsIsReturned() {
        Long id = 100L;

        OverflowDto overflow = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1));
        List<OverflowDto> overflowList = Arrays.asList(overflow);

        List<OverflowDto> overflowList2 = overflowController.getAllOverflows(id);
        Assert.assertEquals(overflowList.get(0).getAmount(), overflowList2.get(0).getAmount());
    }


}
