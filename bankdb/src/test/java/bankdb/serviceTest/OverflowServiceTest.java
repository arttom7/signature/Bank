package bankdb.serviceTest;

import bankdb.modelDB.Account;
import bankdb.modelDto.OverflowDto;
import bankdb.service.OverflowService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class OverflowServiceTest {

    @Autowired
    private OverflowService overflowService;

    @Test
    public void shouldGetAllOverflowsWhenCorrectDataIsGiven() {
        List<OverflowDto> overflowDtoList = overflowService.getOverflows(1L);
        Long expected = 4060330584716476417L;

        Assert.assertEquals(expected, overflowDtoList.get(0).getReceiverNumber());
    }

    @Test
    public void shouldNotGetAllOverflowsWhenGivenDataIsIncorrect() {
        List<OverflowDto> overflowDtoList = overflowService.getOverflows(0L);
        List<OverflowDto> expectedList = new ArrayList<>();
        Assert.assertEquals(expectedList, overflowDtoList);
    }

    @Test
    public void shouldReturnEnoughBalanceWhenGivenDataIsCorrect() {
        Account account = new Account();
        account.setBalance(new BigDecimal(1000));

        BigDecimal checkingValue = new BigDecimal(500);
        boolean verdict = overflowService.checkIfIsEnoughBalance(account, checkingValue);
        Assert.assertTrue(verdict);
    }

    @Test
    public void shouldReturnNotEnoughBalanceWhenGivenDataIsCorrect() {
        Account account = new Account();
        account.setBalance(new BigDecimal(1000));

        BigDecimal checkingValue = new BigDecimal(5000);
        boolean verdict = overflowService.checkIfIsEnoughBalance(account, checkingValue);
        Assert.assertFalse(verdict);
    }

    @Test
    public void shouldCreateOverflowWhenGivenDataIsCorrect() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1));
        String status = overflowService.createOverflow(overflowDto);

        Assert.assertEquals("Wykonano przelew.", status);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenReceiverNumberIsIncorrect() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1));
        String status = overflowService.createOverflow(overflowDto);

        Assert.assertEquals("Niepoprawny numer konta odbiorcy", status);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenAmountIsTooBig() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(1000000000));
        String status = overflowService.createOverflow(overflowDto);

        Assert.assertEquals("Nie wykonano przelewu, nie ma wystarczających środków na koncie.", status);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenAmountIsNegative() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(-1));
        String status = overflowService.createOverflow(overflowDto);

        Assert.assertEquals("Błędna kwota przelewu", status);
    }

    @Test
    public void shouldNotCreateOverflowWhenGivenAmountIsEqual0() {
        OverflowDto overflowDto = new OverflowDto(9140228979403259905L,
                4060330584716476417L, "Marek", "wł",
                "Gdynia", "Overflow", new BigDecimal(0));
        String status = overflowService.createOverflow(overflowDto);

        Assert.assertEquals("Błędna kwota przelewu", status);
    }
}
