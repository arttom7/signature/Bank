package bankview.service;

import bankview.dtoMapper.UserDtoMapper;
import bankview.model.Account;
import bankview.model.User;
import bankview.modelDto.AccountDto;
import bankview.modelDto.UserDto;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

@Service
public class AccountService {

    private final String createBankAccount = "http://localhost:8080/createBankAccount/";
    private final String getConvertedAmount = "http://localhost:8080/convertAmount/";
    private HttpSession session;
    private UserDtoMapper userDtoMapper;

    public AccountService(HttpSession session, UserDtoMapper userDtoMapper) {
        this.session = session;
        this.userDtoMapper = userDtoMapper;
    }

    public void createBankAccount(String name, Currency currency, BigDecimal balance, boolean isDeposit) {
        AccountDto accountDto = new AccountDto();
        accountDto.setName(name);
        accountDto.setCurrency(currency);
        accountDto.setBalance(balance);
        accountDto.setDepositAccount(isDeposit);

        UserDto userDto = userDtoMapper.apply((User) session.getAttribute("loggedUser"));
        String id = userDto.getId().toString();
        String url = createBankAccount + id;

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.postForObject(url, accountDto, AccountDto.class);
    }

    public BigDecimal sumBalance(List<Account> accountList) {
        Currency currency = Currency.getInstance("PLN");
        BigDecimal sum = new BigDecimal(0);
        for (Account account : accountList) {
            if (!account.getCurrency().equals(currency)) {
                BigDecimal convertedSum = convertedAmount(account.getBalance(), account.getCurrency());
                sum = sum.add(convertedSum);
            } else {
                sum = sum.add(account.getBalance());
            }
        }
        return sum;
    }

    public BigDecimal convertedAmount(BigDecimal balance, Currency currency) {
        RestTemplate restTemplate = new RestTemplate();
        String url = getConvertedAmount + balance + "/" + currency;
        BigDecimal sum = restTemplate.getForObject(url, BigDecimal.class);
        return sum;
    }
}
