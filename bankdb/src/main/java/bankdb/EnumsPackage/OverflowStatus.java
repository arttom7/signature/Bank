package bankdb.EnumsPackage;

public enum OverflowStatus {
    Wykonano("Wykonano"),
    WykonanoPrzelew("Wykonano przelew."),
    NieWykonanoPrzelewu("Nie wykonano przelewu, nie ma wystarczających środków na koncie."),
    BłędnaKwotaPrzelewu("Błędna kwota przelewu"),
    NieWykonano("Nie wykonano"),
    NiepoprawnyNumerKontaOdbiorcy("Niepoprawny numer konta odbiorcy");


    private String description;

    public String getDescription() {
        return description;
    }

    OverflowStatus(String description) {
        this.description = description;
    }
}
