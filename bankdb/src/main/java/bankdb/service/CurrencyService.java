package bankdb.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Currency;

@Service
public class CurrencyService {

    public BigDecimal getExchangeRate(Currency senderCurrency, Currency receiverCurrency) {
        RestTemplate restTemplate = new RestTemplate();
        String currencyPair = senderCurrency + "_" + receiverCurrency;
        String url = "https://free.currencyconverterapi.com/api/v6/convert?q=" + currencyPair + "&compact=y";
        JsonNode jsonNode = restTemplate.getForObject(url, JsonNode.class);
        JsonNode exchangeRate = jsonNode.get(currencyPair).get("val");
        BigDecimal rate = exchangeRate.decimalValue();
        return rate;
    }

    public BigDecimal getExchangeRate(Currency currency) {
        RestTemplate restTemplate = new RestTemplate();
        String currencyPair = "PLN" + "_" + currency;
        String url = "https://free.currencyconverterapi.com/api/v6/convert?q=" + currencyPair + "&compact=y";
        JsonNode jsonNode = restTemplate.getForObject(url, JsonNode.class);
        JsonNode exchangeRate = jsonNode.get(currencyPair).get("val");
        BigDecimal rate = exchangeRate.decimalValue();
        return rate;
    }

    public BigDecimal getConvertedAmount(BigDecimal balance, Currency currency) {
        RestTemplate restTemplate = new RestTemplate();
        String currencyPair = currency + "_" + "PLN";
        String url = "https://free.currencyconverterapi.com/api/v6/convert?q=" + currencyPair + "&compact=y";
        JsonNode jsonNode = restTemplate.getForObject(url, JsonNode.class);
        JsonNode exchangeRate = jsonNode.get(currencyPair).get("val");
        BigDecimal rate = exchangeRate.decimalValue();
        BigDecimal convertedValue = balance.multiply(rate);
        return convertedValue;

    }
}
