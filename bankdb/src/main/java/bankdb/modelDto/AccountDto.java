package bankdb.modelDto;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

public class AccountDto {
    private String name;
    private Long id;
    private Long number;
    private Date creationDate;
    private Currency currency;
    private BigDecimal balance;
    private boolean isDepositAccount;

    public AccountDto() {
    }

    public AccountDto(String name, Long id, Long number, Date creationDate, Currency currency, BigDecimal balance, boolean isDepositAccount) {
        this.id = id;
        this.number = number;
        this.creationDate = creationDate;
        this.currency = currency;
        this.balance = balance;
        this.isDepositAccount = isDepositAccount;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean isDepositAccount() {
        return isDepositAccount;
    }

    public void setDepositAccount(boolean depositAccount) {
        isDepositAccount = depositAccount;
    }

}
