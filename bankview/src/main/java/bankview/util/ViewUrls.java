package bankview.util;

public class ViewUrls {
    public static final String HOME_PAGE = "home";
    public static final String REGISTRATION_PAGE = "registration";
    public static final String LOGGING_PAGE = "logging";
    public static final String LOGGEDHOME_PAGE = "loggedHome";
    public static final String ACCOUNT_SETTINGS_PAGE = "accountSettings";
    public static final String CREATING_BANK_ACCOUNT_PAGE = "createBankAccount";
    public static final String MY_BANK_ACCOUNTS_PAGE = "myBankAccounts";
    public static final String MY_FINANCE_PAGE = "myFinance";
    public static final String CREATE_OVERFLOW_PAGE = "createOverflow";
    public static final String CONTACT_PAGE = "contact";
    public static final String HISTORY_PAGE = "financeHistory";
}
