package bankview.dtoMapper;

import bankview.model.Account;
import bankview.modelDto.AccountDto;
import org.springframework.stereotype.Component;

import java.util.function.Function;


@Component
public class AccountDtoMapper implements Function<Account, AccountDto> {
    @Override
    public AccountDto apply(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(account.getId());
        accountDto.setBalance(account.getBalance());
        accountDto.setCreationDate(account.getCreationDate());
        accountDto.setCurrency(account.getCurrency());
        accountDto.setDepositAccount(account.isDepositAccount());
        accountDto.setNumber(account.getNumber());
        accountDto.setName(account.getName());
        return accountDto;
    }
}
