package bankdb.repositoryTest;

import bankdb.modelDB.Overflow;
import bankdb.modelDB.User;
import bankdb.repository.OverflowRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class OverflowRepositoryTest {

    @MockBean
    private OverflowRepository overflowRepository;

    private User user;


    @Before
    public void setUp() {
        Overflow overflow = new Overflow();
        overflow.setStatus("Wykonano");

        List<Overflow> overflowList = Arrays.asList(overflow);
        Mockito.when(overflowRepository.findByUserListContaining(user)).thenReturn(overflowList);
    }

    @Test
    public void shouldReturnListOfOverflowsWhenGivenUserIsCorrect() {
        List<Overflow> overflowList = overflowRepository.findByUserListContaining(user);
        Assert.assertEquals("Wykonano", overflowList.get(0).getStatus());
    }

    @Test
    public void shouldNotReturnListOfOverflowsWhenGivenUserIsIncorrect() {
        User failUser = new User();
        List<Overflow> overflowList = overflowRepository.findByUserListContaining(failUser);
        Assert.assertEquals(0, overflowList.size());
    }
}
