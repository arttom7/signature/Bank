package bankdb.repository;

import bankdb.modelDB.Account;
import bankdb.modelDB.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    List<Account> findByUser(User user);

    List<Account> findAllByNumber(Long number);

    Account findByNumber(Long number);
}
